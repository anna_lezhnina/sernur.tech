import React from "react";
import Layout from "../../components/Layout";
import Header from "../../components/Layout/Header";
import Footer from "../../components/Layout/Footer";
import Logo from "../../components/Logo";
import HeadingLogo from "../../components/HeaderLogo";

const MainPage = props => {
  return (
    <Layout
      header={
        <Header
          navigationItems={[
            { text: "мы", anchor: "#" },
            { text: "проекты", anchor: "#" },
            { text: "контакты", anchor: "#" }
          ]}
          logo={<Logo width="148" marginLeft="-17px" />}
        />
      }
      footer={
        <Footer
          sections={{
            hashtags: ["#sernurtech", "#sernurdesign"],
            copyrights: ['ООО "Сернурские технологии"', "Sernur.tech 2020 ©"],
            sites: ["Kozi.ru", "Sernur.club"]
          }}
        />
      }
    >
      <HeadingLogo />
    </Layout>
  );
};

export default MainPage;
