import React from "react";
import Helmet from "react-helmet";
import MainPage from "./pages/MainPage";
import { ThemeProvider } from "styled-components";
import styled from "styled-components";
import { blackTheme } from "./theme";

const theme = blackTheme;

const Background = styled.div`
  display: flex;
  background: radial-gradient(77.01% 50.06% at 8.68% 0%, #424242 0%, #1A1B1E 100%);
  flex-direction: row;
  justify-content: center;
`;

function Site() {
  return (
    <ThemeProvider theme={theme}>
      <Helmet title="Sernur.tech" />
      <Background>
        <MainPage />
      </Background>
    </ThemeProvider>
  );
}

export default Site;
