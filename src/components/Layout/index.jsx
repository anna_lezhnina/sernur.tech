import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

const StyledContentDiv = styled.div`
  display: flex;
  flex: 1;
  margin: 59px 82px;
  max-width: 1276px;
  flex-direction: column;
`;

const StyledMain = styled.main`
  display: flex;
  flex: 1;
  flex-direction: column;
`;

const Layout = ({ children, header, footer }) => {
  return (
    <StyledContentDiv>
      {header}
      <StyledMain>{children}</StyledMain>
      {footer}
    </StyledContentDiv>
  );
};

Layout.propTypes = {
  header: PropTypes.element.isRequired,
  footer: PropTypes.element.isRequired
};

export default Layout;
